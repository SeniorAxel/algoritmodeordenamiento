/* Nombre: Alexander Ramírez. C.i:30.037.426 
 * Algoritmo optimizado Quick Sort */
public class Quicksortoptimized {
    public static void main(String[] args){

        System.out.println("Mostrando el arreglo numerico ingresado:");
        
        int[] arreglo = {11, 1, 0, 5, 8, -5, 9};

        for(int i = 0; i < arreglo.length; i++){
            System.out.print(arreglo[i] + " ");
        }
        System.out.print("\n");

        selectOrdenamiento(arreglo, 0, arreglo.length - 1);
    
        System.out.println("\nMostrando el arreglo numerico ordenado:");

        for(int i = 0; i < arreglo.length; i++){
            System.out.print(arreglo[i] + " ");
        }
    }

    /*El algoritmo comienza verificando si el tamaño del subarreglo es superior o inferior a 5
     * si es inferior se realiza el metodo de ordenamiento Insertion Sort, 
     * caso contrario realiza el algoritmo Quick sort.*/

    public static void selectOrdenamiento(int[] array, int l, int r){

        if(r - l < 5){

            insertionSort(array, l, r);
            System.out.println("\n___Usando el metodo de ordenamiento Insertion Sort___");
            return;

        }else{

            particionLomito(array, l, r);
            System.out.println("\n___Usando el metodo de ordenamiento Quick Sort___");
        }   
    }

    /*Verificado el tamaño del subarreglo menos a 5 se realiza el ordenamiento Insertion Sort.*/
    public static void insertionSort(int[] a, int l, int r){

        for(int i = l + 1; i <= r; i++){  //Con el siguiente bucle se selecciona un valor del lado derecho del subarreglo
            int n = a[i];                 //luego se procede a insertar en la posicion correcta en el subarreglo del lado izquierdo los valores menores
            int j = i - 1;                // y del lado derecho los valores mayores.
            
            while(j >= l && a[j] > n){
                a[j + 1] = a[j];
                j--;
            }

            a[j + 1] = n;
        }
    }
    
    /*Verifcando que el tamaño del subarreglo es mayor a 5 se realiza el ordenamiento Quicksort
     * pero usando la particion lomuto.*/

    public static void particionLomito(int[] a, int l, int r){
        
        if(l < r){
            int pvt = a[r];
            int i = l - 1;
            
            for(int j = l; j < r; j++){ //divide el arreglo en dos subarreglos utilizando el pivote como referencia.
                if(a[j] <= pvt) {
                    i++;
                    cambiazo(a, i, j);
                }
            }

            cambiazo(a, i + 1, r); //Se realiza un llamado a la funcion, donde los elementos menores o iguales al pivote quedan a su izquierda
                                   // y los elementos mayores quedan a su derecha.
            
            particionLomito(a, l, i); //Se procede a llamar recursivamente la funcion a sí misma para particionar cada subarreglo izquierdo
            particionLomito(a, i + 1, r); //y el subarreglo derecho de forma que todos los elementos esten ordenados.
        }
    }

    //Esta funcion realiza el cambio de posicion del los valores en relacion al pivote escogido
    //los valores menores pasan al subareglo izquiero y los valores mayores pasan al subarreglo derecho. 

    public static void cambiazo(int[] x, int l, int r){
    
        int n = x[l];
        x[l] = x[r];
        x[r] = n;
    }   
}